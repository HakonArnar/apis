const cors = require('cors');
const express = require('express');
const fetch = require('node-fetch');

const app = express()
app.use(cors())


app.get('/api1', (req, res) => {
    fetch("https://NasaAPIdimasV1.p.rapidapi.com/getPictureOfTheDay", {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Rapidapi-Key": "5d54724286mshd1c147c47c2894fp1ee836jsn56baa3f725d7"
        },
        method: "POST"
    })

    .then(r=>r.json())
    .then(j=>{
        console.log("data is ready");
        res.json(j)
    //hér gerum við eitthvað við gögnin okkar fínu
    })
})

/*
=== API2 ===
*/
app.get('/api2', (req, res) => {
    fetch("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/quickAnswer?q=How+much+vitamin+c+is+in+2+apples%3F", {
        headers: {
            "X-Rapidapi-Key": "5d54724286mshd1c147c47c2894fp1ee836jsn56baa3f725d7"
        }
    })
    .then(r=>r.json())
    .then(j=>{
        console.log("data2 is ready");
        res.json(j.answer)
    //hér gerum við eitthvað við gögnin okkar fínu
    })
})


const port = 3000


app.listen(port, () => console.log(`Example app listening on port ${port}!`))

